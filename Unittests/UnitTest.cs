// <copyright file="UnitTest.cs" company="Monosoft ApS">
// Copyright 2019 Monosoft ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Unittests
{
    using System;
    using System.Collections.Generic;
    using ITUtil.Common.Base;
    using Monosoft.Service.Zip2City.V1;
    using Monosoft.Service.Zip2City.V1.Commands;
    using NUnit.Framework;

    /// <summary>
    /// UnitTests.
    /// </summary>
    [TestFixture]
    public class UnitTest
    {
        /// <summary>
        /// GetCityNameFromRange.
        /// </summary>
        [Test]
        public void GetCityNameFromRange()
        {
            Monosoft.Service.Zip2City.V1.DTO.Countryinfo contryinfo = new Monosoft.Service.Zip2City.V1.DTO.Countryinfo();
            contryinfo.country = "denmark";
            contryinfo.zipCode = "1000";

            Get getone = new Get();
            var res = getone.Execute(contryinfo);
            Assert.AreEqual(res.CityName, "KÝbenhavn K");
            Assert.AreEqual(res.ZipCode, "1000");
        }

        /// <summary>
        /// GetCityName.
        /// </summary>
        [Test]
        public void GetCityName()
        {
            Monosoft.Service.Zip2City.V1.DTO.Countryinfo contryinfo = new Monosoft.Service.Zip2City.V1.DTO.Countryinfo();
            contryinfo.country = "denmark";
            contryinfo.zipCode = "8500";

            Get getone = new Get();
            var res = getone.Execute(contryinfo);
            Assert.AreEqual(res.CityName, "Grenaa");
            Assert.AreEqual(res.ZipCode, "8500");
        }

        /// <summary>
        /// Creat Exists Plugin.
        /// </summary>
        [Test]
        public void GetCityNameNotExists()
        {
            Monosoft.Service.Zip2City.V1.DTO.Countryinfo contryinfo = new Monosoft.Service.Zip2City.V1.DTO.Countryinfo();
            contryinfo.country = "denmark";
            contryinfo.zipCode = "0000";

            Get getone = new Get();
            Assert.Throws<ITUtil.Common.Base.ElementDoesNotExistsException>(() => getone.Execute(contryinfo));
        }
    }
}