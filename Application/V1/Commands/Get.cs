﻿// <copyright file="Get.cs" company="Monosoft ApS">
// Copyright 2019 Monosoft ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.Zip2City.V1.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;

    public class Get : GetCommand<DTO.Countryinfo, DTO.ZipCodeInfo>
    {
        public Get() : base("Get a city name from from country and zipcode")
        {
        }

        /// <summary>
        /// Execute.
        /// </summary>
        /// <param name="input">object input.</param>
        /// <returns>List DTO.Fragment.</returns>
        public override DTO.ZipCodeInfo Execute(DTO.Countryinfo input)
        {
            Logic logic = new Logic();
            var res = logic.Get(input);

            return res;
        }
    }
}