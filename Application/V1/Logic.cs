﻿// <copyright file="DBContext.cs" company="Monosoft ApS">
// Copyright 2019 Monosoft ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.Zip2City.V1
{
    using ITUtil.Common.Base;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class Logic
    {
        public DTO.ZipCodeInfo Get(DTO.Countryinfo zipCodeinfo)
        {
            if (zipCodeinfo == null)
            {
                throw new ArgumentNullException(nameof(zipCodeinfo));
            }

            var countries = new List<Database.CountryInfo>(Database.CountryInfo.countrylist);
            var res = new DTO.ZipCodeInfo();
            var countryInfo = countries.FirstOrDefault(x => x.country.ToLower() == zipCodeinfo.country.ToLower());

            if (countryInfo == null)
            {
                throw new ElementDoesNotExistException("Country does not exist", zipCodeinfo.country);
            }

            foreach (var city in countryInfo.zipcodeinfo)
            {
                var cityRange = string.Compare(city.zipCodeRangeStart, "0");
                if (cityRange > 0)
                {
                    var range = string.Compare(city.zipCodeRangeStart, zipCodeinfo.zipCode);
                    var zipcode = string.Compare(city.zipCode, zipCodeinfo.zipCode);
                    if (range <= 0 && zipcode >= 0)
                    {
                        res = new DTO.ZipCodeInfo(zipCodeinfo.zipCode, city.cityName);
                    }
                }
                else if (zipCodeinfo.zipCode == city.zipCode)
                {
                    res = new DTO.ZipCodeInfo(zipCodeinfo.zipCode, city.cityName);
                }
            }

            if (res.Equals(new DTO.ZipCodeInfo()))
            {
                throw new ElementDoesNotExistException("Zipcode does not exist", zipCodeinfo.zipCode.ToString());
            }

            return res;
        }
    }
}
